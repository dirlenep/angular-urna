export interface Voto {
  id?: number;
  rg: number;
  candidato: number;
  timestamp?: Date;
}
