import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppUrna } from "./urna/app-urna.component";
import { VotacaoComponent } from "./votacao/votacao.component";

export const routes: Routes = [
  { path: "", redirectTo: "urna", pathMatch: "full" }, // rota raíz
  { path: "urna", component: AppUrna }, // a rota raíz redireciona para essa rota
  { path: "votos", component: VotacaoComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
