import { Component, Input, OnInit } from '@angular/core';
import { Voto } from 'models/voto.models';
import { VotacaoService } from '../service/votacao.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-votacao',
  templateUrl: './votacao.component.html',
  styleUrls: ['./votacao.component.scss']
})

export class VotacaoComponent implements OnInit {
  votos: any[] = [];

  //@Input() votos: any[] = []; // preparação para receber dados que vem de outro componente

  constructor(private service: VotacaoService, private router: Router) {

  }

  ngOnInit(): void {
    //this.votos = this.service.listaVotos // recepção dos dados do service
    this.service.getAllVotos().subscribe((votosServer: Voto[]) => {
      console.table(votosServer)
      this.votos = votosServer
    })

    setTimeout(() => {
      this.router.navigate(['/']);
    }, 2000);
  }
}
