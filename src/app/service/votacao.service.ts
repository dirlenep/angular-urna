import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Voto } from './../../../models/voto.models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VotacaoService {

  private _listaVotos: any[] = [];
  private url = "http://localhost:3000/votos";

  public addVoto(voto: any): Observable<Voto> {
    //this.validar(voto)
    //this.listaVotos.push(voto)

    return this.httpClient.post<Voto>(this.url, voto)
  }

  //private validar(voto: any){
  //voto.data = new Date();
  // local onde colocaríamos nossas regras de negócios, limite de horário, limite de rg, etc
  //}

  constructor(private httpClient: HttpClient) { }

  public getAllVotos(): Observable<Voto[]> {
    return this.httpClient.get<Voto[]>(this.url) //vai fazer a requisição para o backend
  }

  public get listaVotos(): any[] {
    return this._listaVotos;
  }
  public set listaVotos(value: any[]) {
    this._listaVotos = value;
  }

}
