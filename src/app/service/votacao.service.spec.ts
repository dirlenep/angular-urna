/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { VotacaoService } from './votacao.service';

describe('Service: Votacao', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VotacaoService]
    });
  });

  it('should ...', inject([VotacaoService], (service: VotacaoService) => {
    expect(service).toBeTruthy();
  }));
});
