import { Component } from '@angular/core';
import { VotacaoService } from './service/votacao.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'urna-eletronica';

  //votos: any[] = []; // Criada para receber o objeto que veio de app-urna

  constructor(private service: VotacaoService) { }

  /*
  public votar($event: any) {
    console.log($event);

    //const votos = {...$event, data: new Date()} //é a mesma coisa que fazer:
    //const voto = {$event.rg, $event.candidato, new Date()}

    //this.votos.push($event); // atribuir o objeto a variavel criada anteriormente

    //this.service.addVoto(votos) // atribuir os votos ao método lá do service
  }*/
}

