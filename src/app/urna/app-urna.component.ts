import { Component, Output, EventEmitter } from "@angular/core";
import { VotacaoService } from "../service/votacao.service";

@Component({
  selector: "app-urna",
  templateUrl: "./app-urna.component.html",
  styleUrls: ["./app-urna.component.scss"]
})

export class AppUrna {

  //@Output() aoVotar = new EventEmitter<any>() // Para poder usar fora da pasta urna

  rg: number = 0;
  candidato: number = 0;

  constructor(private service: VotacaoService) { }

  public votar() {
    const data = new Date()
    const voto: object = { rg: this.rg, candidato: this.candidato, timestamp: data };

    //this.aoVotar.emit(voto);
    this.service.addVoto(voto).subscribe(resultado => {
      console.log(resultado);

      this.resetar();
      setTimeout(function () {
        document.location.reload(); // Atualizar a página após o voto
      }, 1000);
    }, error => {
      console.error(error);
    }); // Para poder usar fora da pasta urna v2
  }

  private resetar() {
    this.rg = 0;
    this.candidato = 0;
  }
}
