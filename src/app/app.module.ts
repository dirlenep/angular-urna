import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; // Import para usar o htpp
import { FormsModule } from '@angular/forms'; // Import para usar o form
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppUrna } from './urna/app-urna.component';
import { VotacaoComponent } from './votacao/votacao.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    AppUrna,
    VotacaoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
